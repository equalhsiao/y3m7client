'''
Created on 2016年10月14日

@author: equal
'''
from django.contrib import auth  # 別忘了import auth
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
def index(request):
    return render_to_response('index.html',locals())

def login(request):

    if request.user.is_authenticated(): 
        return HttpResponseRedirect('/index/')

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    
    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect('/index/')
    else:
        return render_to_response('login.html') 
    
    
# def logout(request):
#     auth.logout(request)
#     return HttpResponseRedirect('/index/')