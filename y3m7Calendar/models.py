import mongoengine


# connect('course_db', host='y3m7.ddns.net', port=27017)
# 課程模組
class Course(mongoengine.document.Document):
    CourseId = mongoengine.StringField(max_length=100);
    TrainingDateRange = mongoengine.StringField(max_length=100);
    City = mongoengine.StringField(max_length=100);
    City = mongoengine.StringField(max_length=100);
    TrainingType = mongoengine.StringField(max_length=100);
    CourseNo = mongoengine.StringField(max_length=100);
    OCName = mongoengine.StringField(max_length=100);
    OName = mongoengine.StringField(max_length=100);
    SignupQuota = mongoengine.StringField(max_length=100);
    InsuranceNo = mongoengine.StringField(max_length=100);
    CourseName = mongoengine.StringField(max_length=100);
    ClassHour = mongoengine.StringField(max_length=100);
    OAddress = mongoengine.StringField(max_length=100);
    TrainingEndDate = mongoengine.StringField(max_length=100);
    TrainingStartDate = mongoengine.StringField(max_length=100);
    ClassType = mongoengine.StringField(max_length=100);
    StudentPay = mongoengine.StringField(max_length=100);
    ClassQuota = mongoengine.StringField(max_length=100);
    SignupEndDate = mongoengine.StringField(max_length=100);
    GovPay = mongoengine.StringField(max_length=100);
    OTel = mongoengine.StringField(max_length=100);
    SignupStartDate = mongoengine.IntField(max_length=100);
    meta = {'collection' : 'course'}
#     meta = {"db_alias": "COURSE_MASTER"} 
#     , 'clooection': 'course_db' 
class Test(mongoengine.document.Document):
    url = mongoengine.StringField(max_length=100)
    name= mongoengine.StringField(max_length=100)
