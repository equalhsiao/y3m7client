'''
Created on 2016年7月16日

@author: equal
'''
from django.conf.urls import url
from y3m7Calendar import views


# router = routers.SimpleRouter()
# router.register(r'users', views.UserViewSet,base_name='users')
# router.register(r'groups', views.GroupViewSet,base_name='groups')

#註url後面一定要加上$才是完整名稱mapping否則會因為符合跳到其他
urlpatterns = [
                url(r'^$', views.IndexView.as_view(), name='index'),
                url(r'^auth_return$', views.IndexView.as_view(), name='auth_return'),
#                 url(r'^home',views.HomeView.as_view(),name='home'),
                #查詢課表
                url(r'^api/Course$',views.CourseViewSet.as_view(),name="Course"),
                #使用者授權uri
                #url(r'^api/AuthLogin*',views.CalendarAuthViewSet.as_view()),
                #url(r'^callback/\?code',views.CalendarAuthViewSet.as_view()),
                #calendarList Uri
                url(r'^api/CalendarList$',views.CalendarListViewSet.as_view(),name="CalendarList"),
                #calendars Uri
                url(r'^api/Calendars$',views.CalendarsViewSet.as_view(),name="Calendars"),
                #event Uri
                url(r'^api/Events$',views.EventsViewSet.as_view(),name="Events"),
                #使用者操作資訊
                url(r'^api/UserInfo$',views.UserAddCourse.as_view(),name="UserAddCourse"),
                #課程明細
                url(r'^api/CourseInfo$',views.CourseInfo.as_view(),name="CourseInfo"),
#                 url(r'^queryset$',views.query)
                ]