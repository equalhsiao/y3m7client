from asyncio.log import logger
import datetime
import json
from lib2to3.fixer_util import String
from time import sleep
from urllib.request import urlopen

from bs4 import BeautifulSoup
from bson.json_util import dumps
from bson.objectid import ObjectId
from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError
from django.views import generic
from googleapiclient import discovery
from googleapiclient.errors import HttpError
import httplib2
from mongoengine.connection import connect
from oauth2client.client import AccessTokenCredentials, FlowExchangeError, \
    AccessTokenCredentialsError, flow_from_clientsecrets
from pymongo.mongo_client import MongoClient
import pytz
from rest_framework.response import Response
from rest_framework.views import APIView

from dao import CourseMessage, CourseScore
from y3m7Calendar.models import Course
from y3m7Client.settings import GOOGLE_OAUTH2_CLIENT_SECRETS_JSON, Y3M7_CALLBACK_URL


# from dao.CourseScore import CourseScore
connect(host='m7y3.ddns.net', port=27017)
uri = "mongodb://m7y3CalendarAdmin:nightgossoon90161@m7y3.ddns.net:27017/course_db" 
client = MongoClient(uri)
db = client.course_db
HOME_URI = "/y3m7Calendar/api/AuthLogin"
redirect_uri = str(Y3M7_CALLBACK_URL)+'/callback/';
CLIENT_SECRET_FILE = GOOGLE_OAUTH2_CLIENT_SECRETS_JSON
service = None;
FLOW = flow_from_clientsecrets(CLIENT_SECRET_FILE,
                               scope='https://www.googleapis.com/auth/calendar',
                               redirect_uri=str(Y3M7_CALLBACK_URL))
EmptyObject = {"rc":"0000","rm":"success"};


def getRemoteIp(self,request,page):
        remoteDateTime = datetime.datetime.now().strftime("%Y%m%d %H%M%S").split(" ");
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        date = remoteDateTime[0];
        time = remoteDateTime[1];
        access = None;
        ip = None;
        if x_forwarded_for:
            ip = str(x_forwarded_for.split(',')[-1].strip())
            access = {"page":page,"forward":"FORWARDED_FOR","ip":str(x_forwarded_for.split(',')[-1].strip()),
                      "date":date,"time":time};
        elif request.META.get('HTTP_X_REAL_IP'):
            ip = str(str(request.META.get('HTTP_X_REAL_IP')))
            access = {"page":page,"forward":"REAL_IP","ip":str(request.META.get('HTTP_X_REAL_IP')),
                      "date":date,"time":time};
        else:
            ip = str(request.META.get('REMOTE_ADDR'))
            access = {"page":"calednar","forward":"REMOTE_ADDR","ip":str(request.META.get('REMOTE_ADDR')),
                      "date":date,"time":time};
        db.ACCESS_LOG.insert_one(access);
        return ip
    
    
class IndexView(generic.ListView):
    template_name = 'y3m7Calendar/index.html'
    context_object_name = 'latest_course_list'
    
    def defaultQuery(self):
        logger.info("method=defaultQuery");
        resp = list();
        test = db.COURSE_MASTER.find({"CourseId":"88456"}) 
        for count in range(0,test.count(),1):
                course = Course;
                course.OName = test[count]["OName"];
                course.CourseId = test[count]["CourseId"];
                course.CourseName = test[count]["CourseName"];
                course.OTel = test[count]["OTel"];
                course.City = test[count]["City"];
                resp.append(course)
        return resp
    
    def get_queryset(self,queryCondition):
        logger.info("method=get_queryset");
        resp = list();
        result = db.COURSE_MASTER.find(queryCondition.__dict__)
        for count in range(0,result.count(),1):
                course = Course();
                course.OName = result[count]["OName"];
                print(course.OName)
                course.CourseId = result[count]["CourseId"];
                print(course.CourseId)
                course.CourseName = result[count]["CourseName"];
                course.OTel = result[count]["OTel"];
                course.City = result[count]["City"]
                resp.append(course)
        return resp
    def get(self, request, *args, **kwargs):
        getRemoteIp(self,request=request,page="calendarIndex")
        code = None;
        try:
            code = request.GET['code'];
        except MultiValueDictKeyError as e:
            authorize_url = FLOW.step1_get_authorize_url()
            return redirect(authorize_url)
        credentials  = None;
        try:
            credentials = FLOW.step2_exchange(code=code)
        except FlowExchangeError as e:
            authorize_url = FLOW.step1_get_authorize_url()
            return redirect(authorize_url)
        
        request.session['access_token']= credentials.access_token
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('calendar', 'v3', http=http)
        page_token = None;
        calendar_list = service.calendarList().list(pageToken=page_token).execute()
        print(calendar_list)
        request.session['etag'] = calendar_list["etag"];
        print("etag:"+request.session['etag'])
        calendars = list();
        for calendar_list_entry in calendar_list['items']:
                calendarResposne = CalendarListResponseDto();
                calendarResposne.setSummary(calendar_list_entry['summary'])
                calendarResposne.setCalendarId(calendar_list_entry['id'])
                calendars.append(calendarResposne)
        if request.method == 'GET':
            for key in request.GET:
                if key != "":
                    print(request.GET[key])
        CourseId = "";
        print(request.GET)
        if len(request.GET) <= 1 :
            context = {'calendarList':calendars,'reqParams':request.GET,'latest_course_list': self.defaultQuery()}
        else: 
            queryCondition = CourseRequestDto();
            if "CourseId" in request.GET:
                queryCondition.setCourseId(request.GET["CourseId"])
            if "CourseName" in request.GET:
                queryCondition.setCourseName(request.GET["CourseName"])
            if ("City" in request.GET) and (len(request.GET["City"]) >= 0):
                queryCondition.setCity(request.GET["City"])
            startDate = int(request.GET['startDate']);
            endDate = int(request.GET['endDate']);
            queryCondition.setTrainingStartDate(startDate, endDate)
            print("開課時間")    
            
            context = {'reqParams':request.GET,'latest_course_list': self.get_queryset(queryCondition)}
            print("共"+context.get("latest_course_list").count()+"筆");
        return render(request,self.template_name,context);
class CalendarListResponseDto:
    def setSummary(self,summary):
        self.summary = summary;
    def setCalendarId(self,calendarId):
        self.calendarId = calendarId;
        
class CourseRequestDto:
    def setCourseId(self,CourseId):
        self.CourseId = CourseId;
    def setCity(self,CityList):
        
        print(CityList)
        querylist = list();
        for city in CityList:
            querylist.append(city);
        self.City = {"$in":CityList}
    def setCourseName(self,CourseName):
        self.CourseName ={"$regex": CourseName};
    def setTrainingStartDate(self,startDate,endDate):
        self.TrainingStartDate = {"$gt":startDate,"$lt":endDate};
class CourseResponseDto:
    def setOName(self,OName):
        self.OName = OName;
    def setCourseId(self,CourseId):
        self.CourseId = CourseId;
    def setCourseName(self,CourseName):
        self.CourseName = CourseName;
    def setOTel(self,OTel):
        self.OTel = OTel;
    def setCity(self,City):
        self.City = City;
class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
class CourseViewSet(APIView):
    def get_queryset(self,queryCondition):
        logger.info("method=get_queryset");
        resp = list();
        result = db.COURSE_MASTER.find(queryCondition.__dict__);
        for count in range(0,result.count(),1):
                OName = result[count]["OName"];
                CourseId = result[count]["CourseId"]
                CourseName = result[count]["CourseName"];
                OTel = result[count]["OTel"];
                City = result[count]["City"];
                TrainingStartDate = result[count]["TrainingStartDate"];
                TrainingEndDate = result[count]["TrainingEndDate"];
                OAddress = result[count]["OAddress"];
                course = {"OName":OName,"CourseId":CourseId,"CourseName":CourseName,
                          "OTel":OTel,"City":City,"TrainingStartDate":TrainingStartDate,
                          "TrainingEndDate":TrainingEndDate,"OAddress":OAddress}
                resp.append(course)
        return resp;
    
    
    def post(self, request): 
        print(request.method)
        queryCondition = CourseRequestDto();
        if "CourseId" in request.data:
            print(request.data["CourseId"])
            queryCondition.setCourseId(request.data["CourseId"])
        if "CourseName" in request.data:
            print(request.data["CourseName"])
            queryCondition.setCourseName(request.data["CourseName"])
            
        if "City" in request.data:
                print(request.data["City"])
                queryCondition.setCity(request.data["City"]) 
        startDate = int(request.data['startDate']);
        endDate = int(request.data['endDate']);
        queryCondition.setTrainingStartDate(startDate, endDate)
        print("開課時間")    
        print(request.data['startDate'])
        print(request.data['endDate'])
        
        print(queryCondition.__dict__)
        context = {'reqParams':request.data,'courseList': self.get_queryset(queryCondition)}
        print("共"+str(len(context.get("courseList")))+"筆");
        return Response(context)



class CalendarAuthViewSet(APIView):
    #取得使用者授權
    def get_credentials(self,request):
        code = request.session['code']
        if code == None:
            authorize_url = FLOW.step1_get_authorize_url()
            redirect(authorize_url)
        else:
            credentials = FLOW.step2_exchange(code)
            print(credentials.client_id)
        return credentials
    def post(self,request):
        print(request.data)
        print("callback post");
        try:
            request.session['code']= request.data['code']
        except:
            return redirect(HOME_URI) 
        print(request.session['code'])
        return Response({"rc":"SUCCESS"});
    def get(self,request):
        print("calendar GET")
        returnUrl = self.get_credentials(request);
        return Response({"method":"GET","returnUrl":returnUrl});

class Events:
    def __init__(self,weekly,startTime):
        self.weekly = weekly;
        self.startTime = startTime;
    def __hash__(self):  
        return hash(str(self.weekly));
    def __eq__(self, other):  
        if isinstance(other, Events):  
            return ((self.weekly == other.weekly)&(self.startTime == other.startTime))  
        else:  
            return False  
    def setWeekly(self,weekly):
        self.weekly = weekly;
    def setStartDate(self,startDate):
        self.startDate = startDate;
    def setEndDate(self,endDate):
        self.endDate = endDate;
    def setCourseName(self,courseName):
        self.courseName = courseName;
    def setRecurrence(self,recurrence):
        self.recurrence = recurrence;
    def setStartTime(self,startTime):
        self.startTime = startTime;
    def setEndTime(self,endTime):
        self.endTime = endTime;
    def setDescription(self,description):
        self.description = description;
class EventsViewSet(APIView):
    SCOPES = 'https://www.googleapis.com/auth/calendar'
    def login(self,request):
            access_token = request.session['access_token'];
            credentials = AccessTokenCredentials(access_token, 'user-agent-value')
            return credentials
    def put(self,request): 
        UserAddCourse.post(self, request);
        calendarId = request.data['calendarId'].strip();
        access_token = request.session['access_token']
        credentials = AccessTokenCredentials(access_token, 'user-agent-value')
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        
        courseId = request.data['courseId']
        result = db.COURSE_MASTER.find({"CourseId":courseId})
        course = Course;
        htmlLink = "http://tims.etraining.gov.tw/timsonline/index3.aspx?OCID="+courseId
        html = urlopen(htmlLink);
        bsObj = BeautifulSoup(html,"html.parser");
        tables = bsObj.find("table",{"id":"Datagrid3"});
        trs = tables.findAll("tr")
        location = None;
        description = None;
        
        for attendees in request.data['attendees']:
            print(attendees);
        for count in range(0,result.count(),1):
                course.OName = result[count]["OName"];
                course.OAddress =result[count]["OAddress"];
                description = "課程名稱:"+str(result[count]["OName"])+"\n" +"課程編號:"+str(result[count]["CourseId"])+"\n" +"網站連結:"+htmlLink
                course.CourseId = result[count]["CourseId"];
                course.TrainingStartDate = result[count]["TrainingStartDate"]
                course.TrainingEndDate = result[count]["TrainingEndDate"];
        courseItems = "";
        eventSet = set();
        for i in range(1,len(trs),1):
            eventObj = None;
            spans = trs[i].findAll("span")
            date = None;
            weekly = None;
            textarea = trs[i].find("textarea").text.strip()
            if(textarea != None and textarea != ""):
                print(textarea)
                courseItems = courseItems + "\n" + textarea;
            for j in range(0,len(spans),1):
                if(spans[j].text.find("(")>0):
                    date = spans[j].text[:spans[j].text.find("(")];
                    startPostion = spans[j].text.find("(");
                    endPostion = len(spans[j].text)
                    weekly = spans[j].text[ startPostion: endPostion]
#目前暫不使用一次性新增完成 因其他課程時間並不一定是連續的
#                     if(weekly =="(星期一)"):
#                         weekly ="MO"
#                     elif(weekly == "(星期二)"):
#                         weekly = "TU"
#                     elif(weekly =="(星期三)"):
#                         weekly ="WE";
#                     elif(weekly == "(星期四)"):
#                         weekly ="TH";
#                     elif(weekly == "(星期五)"):
#                         weekly ="FR";
#                     elif(weekly =="(星期六)"):
#                         weekly ="SA";
#                     elif(weekly =="(星期日)"):
#                         weekly ="SU";
                else:
                    time = spans[j].text.split("~");
                    if(len(time) > 1):
                        
                        startDate = str(date.replace("/","-")) +"T"+ time[0];
                        eventObj = Events(weekly,time[0]);
                        eventObj.setStartDate(startDate);
                        endDate = str(date.replace("/","-")) +"T"+ time[1];
                        eventObj.setEndDate(endDate);
                        eventObj.setEndTime(time[1]);
                        eventSet.add(eventObj);
        for ts in eventSet:
            print(ts.weekly)
        for eventObj in eventSet:
            until = str(course.TrainingEndDate)+"T235959Z"
            event = {
                    'summary': str(course.OName),
              'location': location,
              'description': description+courseItems,
              'start': {
                'dateTime': eventObj.startDate+':00+08:00',
                'timeZone': 'Asia/Taipei',
              },
              'end': {
                'dateTime': eventObj.endDate+':00+08:00',
                'timeZone': 'Asia/Taipei',
              },
              'recurrence': [
                'RRULE:FREQ=WEEKLY;UNTIL='+until+";",
              ],
            'attendees': request.data['attendees'] if 'attendees' in request.data else [],
              'reminders': {
                'useDefault': False,
                'overrides': [
                  {'method': 'email', 'minutes': 24 * 60},
                  {'method': 'popup', 'minutes': 10},
                ],
              },
            }
            try:
                eventResp = service.events().insert(calendarId=calendarId, body=event).execute()
                print(eventResp)
                while(eventResp == None):
                    sleep(1)
                    eventResp = service.events().insert(calendarId=calendarId, body=event).execute()
                print('Event created: %s' % (event["summary"]))
            except HttpError as e:
                e.print_exc()
#                 return Response({"rc":"0001","rm":"當日流量已用完 請稍後在試"});
        return Response({"rc":"0000","rm":"SUCCESS"})
    #查詢最近事件
    def post(self,request):
        calendarId = request.data['calendarId'];
        startDate = request.data['startDate'];
        endDate = request.data['endDate'];
        timeMax = pytz.timezone("Asia/Singapore").localize(datetime.datetime(int(endDate[:4]),int(endDate[4:6]),int(endDate[6:8]))).isoformat();
        timeMin = pytz.timezone("Asia/Singapore").localize(datetime.datetime(int(startDate[:4]),int(startDate[4:6]),int(startDate[6:8]))).isoformat();
#         pytz.timezone("Asia/Singapore").localize
        access_token = request.session['access_token']
        credentials = AccessTokenCredentials(access_token, 'user-agent-value')
        http = httplib2.Http()
        http = credentials.authorize(http)  
        service = discovery.build('calendar', 'v3', http=http)
        events = service.events().list(pageToken=None,calendarId=calendarId,timeMin = timeMin).execute()
        page_token = events.get('nextPageToken')
        return Response(events)
    def delete(self,request):
        print("method DELETE")
        calendarId = request.data['calendarId'];
        eventId = request.data['eventId'];
        print("eventId:")
        print(eventId)
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        try:
            service.events().delete(calendarId=calendarId, eventId=eventId).execute()
        except AccessTokenCredentialsError as e :
            return Response({"0002":"授權已過期"})
        return Response({});
    def get(self,request):
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http()) 
        print(request.method) 
        service = discovery.build('calendar', 'v3', http=http)
        for data in request.GET:
            print(data)
        calendarId = request.GET['calendarId']
        print(calendarId)
        eventId = request.GET['eventId']
        print(eventId)
        event = service.events().get(calendarId=calendarId, eventId=eventId).execute()
        print(event['summary']);
        return Response(event);        
class CalendarsViewSet(APIView):
    SCOPES = 'https://www.googleapis.com/auth/calendar'
    def login(self,request):
        try:
            access_token = request.session['access_token']
            print(access_token)
            credentials = AccessTokenCredentials(access_token, 'user-agent-value')
            return credentials
        except FlowExchangeError as e:
            access_token = request.session['access_token'];
            credentials = AccessTokenCredentials(access_token, 'user-agent-value')
            return credentials
    def delete(self,request):
        print("method DELETE")
        secondaryCalendarId = request.data['secondaryCalendarId'];
        print(secondaryCalendarId)
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        calendarResp = service.calendars().delete(calendarId=secondaryCalendarId).execute()
        return Response({"calendarResp":calendarResp});
    def get(self,request):
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        for data in request.data:
            print(data)
        calendarId = request.GET['calendarId']
        calendar = service.calendars().get(calendarId=calendarId).execute()
        print(calendar['summary']);
        return Response(calendar);    
    def put(self,request): 
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        summary = request.data['summary']
        print(id)
        calendar = {
                'summary': summary,
                'timeZone': 'America/Los_Angeles'
            }
        created_calendar = service.calendars().insert(body=calendar).execute()
        print(created_calendar['id'] )
        return Response({"created_calendar":created_calendar});
    #清空日曆
    def post(self,request):
        print("calendarList POST")
        calendarId = request.data['calendarId'];
        print("calendarId"+str(calendarId));
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)      
        service.calendars().clear(calendarId = calendarId).execute()
        return Response({});  
class CalendarListViewSet(APIView):
    SCOPES = 'https://www.googleapis.com/auth/calendar'
    def login(self,request):
            code = request.session['code']
            print(code)
            if(code == None):
                authorize_url = FLOW.step1_get_authorize_url()
                code = None;
                return Response({"authorize":authorize_url}) 
            else:
                print(code)
                print("what")
                credentials = FLOW.step2_exchange(code=code)
                if(credentials is None):
                    print("credentials is None")
                http = credentials.authorize(httplib2.Http())
            return http
    def post(self,request):
        print("calendarList POST")
        http = self.login(request);
        service = discovery.build('calendar', 'v3', http=http)      
        page_token = None;
        while True:
            calendar_list = None;
            try:
                calendar_list = service.calendarList().list(pageToken=page_token).execute()
            except AccessTokenCredentialsError as e:
                return redirect(HOME_URI)
            print(calendar_list)
            print(calendar_list.get("Contacts"))
            for calendar_list_entry in calendar_list['items']:
                print(calendar_list_entry['summary'])
            if not page_token:
                break
        return Response({"calendarList":calendar_list});
    
    def delete(self,request):
        print("method DELETE")
        calendarId = request.data['calendarId'];
        credentials = self.login(request);
        if(credentials == None):
            return redirect(HOME_URI)   
        
        http = httplib2.Http()
        http = credentials.authorize(http) 
        print("credentials.authorize")
        service = discovery.build('calendar', 'v3', http=http)
        calendarResp = service.calendarList().delete(calendarId=calendarId).execute()
        return Response({"calendarResp":calendarResp});
    def get(self,request):
        getRemoteIp(self,request,"calendarList")
        credentials = self.login(request);
        print(credentials)
        if(credentials == None):
            return redirect(credentials)    
        http = httplib2.Http()
        http = credentials.authorize(http) 
        print("credentials.authorize")  
        service = discovery.build('calendar', 'v3', http=http)
        for data in request.data:
            print(data)
        calendarId = request.GET['calendarId']
        calendar_list_entry = service.calendarList().get(calendarId=calendarId).execute()
        print(calendar_list_entry)
        return Response(calendar_list_entry);
        
    def put(self,request): 
        credentials = self.login(request);
        if(credentials == None | type(credentials) == type(String)):
            return redirect(credentials)    
        http = credentials.authorize(httplib2.Http())  
        service = discovery.build('calendar', 'v3', http=http)
        calendarId = request.data['id']
        print(id)
        calendar_list_entry = {
                'id': calendarId
            }
        created_calendar_list_entry = service.calendarList().insert(body=calendar_list_entry).execute()
        print(created_calendar_list_entry['summary'] ) 
        return Response({"created_calendar_list_entry":created_calendar_list_entry}); 
#使用者點擊新增紀錄       
class UserAddCourse(APIView):
    #新增點擊率
    def post(self,request):
        if "courseId" in request.data:
            courseId = request.data["courseId"];
            attentionCourse = self.get_queryset(courseId);
            if(attentionCourse.count()<= 0):
                db.ATTENTION_COURSE.insert_one({"CourseId":str(courseId),"Popularity":1});
            else:
                Popularity = int(attentionCourse[0]["Popularity"])+1;
                db.ATTENTION_COURSE.update_one({"CourseId":courseId},{"Popularity":int(Popularity)});
        return Response(EmptyObject);
    #查詢點擊率
    def get(self,request):
        if "courseId" in request.data:
            courseId = request.data["courseId"];
            attention = self.get_queryset(courseId);
            if(attention.count()>= 0):
                return Response({"attention":attention})
        return Response({});
    #新增評分
    def put(self,request):
        return Response({})
    #共用查詢
    def get_queryset(self,courseId):
        attention = db.ATTENTION_COURSE.find({"CourseId":courseId});
        return attention;

    
    
class CourseInfo(APIView):
    def get(self,request):
        if ("courseId" in request.GET):
            print(request.GET["courseId"])
            courseId = request.GET["courseId"];
            courseScore = CourseScore.queryScore(courseId);
        return Response(courseScore);
    def post(self,request):
        print("courseScore")
        etag = request.session['etag'];
        print("message" in request.data)
        print("score" in request.data)
        if ("courseId" in request.data) and ("score" in request.data) and ("message" in request.data):
            message = request.data["message"];
            CourseId = request.data["courseId"];
            Score = int(request.data["score"]);
            etag = request.session["etag"];
            print("etag:"+str(etag))
            ip = getRemoteIp(self,request, "Calendar");
            score = CourseScore.queryUserScore(CourseId, etag);
            print("count:",score.count())
            #若評過分就更新評分
            if(score.count() > 0):
                CourseScore.updateCourseScore(CourseId, etag,Score);
            else:
                CourseScore.insertCourseScore(CourseId, Score, ip, etag)
            messageResp = CourseMessage.insertCourseMessage(CourseId, etag, message)
            return Response(messageResp);
        return Response({"rc":"00005","rm":"很抱歉,系統異常"})