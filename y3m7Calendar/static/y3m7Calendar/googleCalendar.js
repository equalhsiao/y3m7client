// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var CLIENT_ID = '543125221790-hpusqso0o0lbj63hqac7f7a61fpliohu.apps.googleusercontent.com';

var SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];

/**
 * Check if current user has authorized this application.
 */
function checkAuth() {
  gapi.auth.authorize(
    {
      'client_id': CLIENT_ID,
      'scope': SCOPES.join(' '),
      'immediate': true
    }, function(){
    	loadCalendarApi();
    });
}
//function getGoogleCalendarList(){
//	gapi.auth.authorize(
//		    {
//		      'client_id': CLIENT_ID,
//		      'scope': SCOPES.join(' '),
//		      'immediate': true
//		    }, function(){
//		    	loadCalendarApi();
//		    });
//}
/**
 * Handle response from authorization server.
 *
 * @param {Object} authResult Authorization result.
 */
function handleAuthResult(authResult) {
  var authorizeDiv = document.getElementById('authorize-div');
  if (authResult && !authResult.error) {
    // Hide auth UI, then load client library.
    authorizeDiv.style.display = 'none';
    loadCalendarApi();
  } else {
    // Show auth UI, allowing the user to initiate authorization by
    // clicking authorize button.
    authorizeDiv.style.display = 'inline';
  }
}

/**
 * Initiate auth flow in response to user clicking authorize button.
 *
 * @param {Event} event Button click event.
 */
function handleAuthClick(event) {
  gapi.auth.authorize(
    {client_id: CLIENT_ID, scope: SCOPES, immediate: false},
    function(){
    	loadCalendarApi();
    });
  return false;
}

/**
 * Load Google Calendar client library. List upcoming events
 * once client library is loaded.
 */
function loadCalendarApi() {
  gapi.client.load('calendar', 'v3', listUpcomingEvents);
}

/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {
	
	console.log((new Date()).toISOString())
  var request = gapi.client.calendar.events.list({
    'calendarId': 'primary',
    'timeMin': '2016-08-30T08:51:00.216Z',
    'showDeleted': false,
    'singleEvents': true,
    'maxResults': 10,
    'orderBy': 'startTime'
  });

  request.execute(function(resp) {
	console.log(resp)
    /*var events = resp.items;
    appendPre('Upcoming events:');

    if (events.length > 0) {
      for (i = 0; i < events.length; i++) {
        var event = events[i];
        var when = event.start.dateTime;
        if (!when) {
          when = event.start.date;
        }
        appendPre(event.summary + ' (' + when + ')')
      }
    } else {
      appendPre('No upcoming events found.');
    }*/

  });
}

/**
 * Append a pre element to the body containing the given message
 * as its text node.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
  var pre = document.getElementById('output');
  var textContent = document.createTextNode(message + '\n');
  pre.appendChild(textContent);
}
 
 

 function insertCalendarApi(obj) {
 	checkAuth();
 	gapi.client.load('calendar', 'v3', insertEventCalendar(obj));
 }
 function insertEventCalendar(obj) {
 	if (obj == null || obj.length <= 0) {
 		console.log("物件不可為空")
 		throw "物件不可為空";
 	}
 	// 導入資料
 	for (var i = 0; i < obj.length; i++) {
 		var event = {
 			'summary' : (function() {
 				if (obj[i].title == undefined || obj[i].title == null) {
 					return "";
 				}
 				return obj[i].title;
 			})(),
 			'location' : (function() {
 				if (obj[i].location == undefined || obj[i].location == null) {
 					return "";
 				}
 				return obj[i].location;
 			})(),
 			'description' : (function() {
 				if (obj[i].description == undefined
 						|| obj[i].description == null) {
 					return " ";
 				}
 				return obj[i].description;
 			})(),
 			'start' : {
 				'date' : (function() {
 					if (obj[i].startDate == undefined
 							|| obj[i].startDate == null) {
 						return (new Date()).toISOString();
 					}
 					return obj[i].startDate;
 				})(),
 				'timeZone' : (function() {
 					if (obj[i].startTimeZone == undefined
 							|| obj[i].startTimeZone == null) {
 						return 'Asia/Taipei';
 					}
 					return obj[i].startTimeZone;
 				})(),
 			},
 			'end' : {
 				'date' : (function() {
 					if (obj[i].endDate == undefined
 							|| obj[i].endDate == null) {
 						return (new Date()).toISOString();
 					}
 					return obj[i].endDate;
 				})(),
 				'timeZone' : (function() {
 					if (obj[i].endTimeZone == undefined
 							|| obj[i].endTimeZone == null) {
 						return 'Asia/Taipei';
 					}
 					return obj[i].endTimeZone;
 				})(),
 			},
 			'reminders' : {
 				'useDefault' : false,
 				'overrides' : [ {
 					'method' : 'email',
 					'minutes' : 24 * 60
 				}, {
 					'method' : 'popup',
 					'minutes' : 10
 				} ]
 			}
 		};
 		var request = gapi.client.calendar.events.insert({
 			'calendarId' : 'primary',
 			'resource' : event
 		});

 		request.execute(function(event) {
 			appendPre('Event created: ' + event.htmlLink);
 		});
 	}
 }