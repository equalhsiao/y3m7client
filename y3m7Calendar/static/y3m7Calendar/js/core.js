/**
 * 
 */

(function init(){
	$("head").append("<script type='text/javascript' src='/static/y3m7Calendar/jquery-loading-overlay-master/src/loadingoverlay.min.js'></script>");
})()

var coreAjax = function(req){
	try{
    $.LoadingOverlay("show");
	$.ajax({
		type : req.method,
		url : req.url,
		async : true, // 使用預設
		cache : false,
		dataType : 'json', // 回來的資料自動以HTML 載入對應div
		contentType : 'application/json',
		data :(function(){
			if(req.method=="GET"){
				return req.params;
			}
			return JSON.stringify(req.params)// 參數轉換為 json object
		})(),
		success :function(data){
			req.success(data);
			$.LoadingOverlay("hide");
		},
		error : function(){
			if(req.error != undefined && $.isfunction(req.error)){
				req.error();
			}
		}
	}).done(function(){
		if(req.done != undefined && $.isfunction(req.done)){
			req.done();
		}
		
	}); 
	}catch(exception){
		console.log(exception.stack);
		alert("系統異常")
	}
}


var dataTableLanguage = {
		"sProcessing" : "處理中...",
		"sLengthMenu" : "顯示 _MENU_ 項結果",
		"sZeroRecords" : "沒有匹配結果",
		"sInfo" : "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
		"sInfoEmpty" : "顯示第 0 至 0 項結果，共 0 項",
		"sInfoFiltered" : "(從 _MAX_ 項結果過濾)",
		"sInfoPostFix" : "",
		"sSearch" : "查詢：",
		"sUrl" : "",
		"oPaginate" : {
			"sFirst" : "|<",
			"sPrevious" : "<<",
			"sNext" : ">>",
			"sLast" : ">|"
		}
	};
