from django.apps import AppConfig


class Y3M7CalendarConfig(AppConfig):
    name = 'y3m7Calendar'
