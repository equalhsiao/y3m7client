from rest_framework_mongoengine.serializers import DocumentSerializer

from y3m7Calendar.models import Course


class CourseSerializer(DocumentSerializer):
    class Meta:
        model = Course
        fields = '__all__'
    def create(self, validated_data):  
        return Course.objects.create(**validated_data)  
   
    def update(self, instance, validated_data):  
        instance.CourseId = validated_data.get('CourseId', instance.CourseId)  
        instance.save()  
        return instance  
