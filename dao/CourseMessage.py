import datetime

from bson.json_util import dumps
from mongoengine.connection import connect
from pymongo.mongo_client import MongoClient


connect(host='m7y3.ddns.net', port=27017)
uri = "mongodb://m7y3CalendarAdmin:nightgossoon90161@m7y3.ddns.net:27017/course_db" 
client = MongoClient(uri)
db = client.course_db
EmptyObject = {"rc":"0000","rm":"留言成功,感謝您"};
NoScore = {"rc":"0005","rm":"NoData"}



# def get_queryset(self,courseId):
#     message = db.COURSE_MESSAGE.find({"CourseId":courseId})
#     return message;
#查詢課程留言
def queryCourseMessage(CourseId,etag):
    userMessage = db.COURSE_MESSAGE.find({"CourseId":CourseId,"Etag":etag})
    return ({"message":userMessage});
#新增課程留言
def insertCourseMessage(CourseId,etag,message):
        print("insertCourseMessage")
        userMessage = db.COURSE_MESSAGE.find({"CourseId":CourseId,"Etag":etag}).sort('_id',-1).limit(-1)
        today =  datetime.datetime.now().strftime("%Y%m%d %H%M%S").split(" ");
        date = today[0];
        time = today[1];
        compareTime = None;
        dbTime = None;
        compareTime = "1"+time;
        print("compareTime:"+compareTime)
        if(userMessage.count()>0):
            print(str(userMessage[0]['time']))
            dbTime= "1"+str(userMessage[0]['time']);
            print("dbTime:"+dbTime)
            if(int(dbTime)+30 <= int(compareTime) and userMessage.count() <= 50):
                print("debug:",date);
                db.COURSE_MESSAGE.insert_one({"CourseId":CourseId,"Etag":etag,"Message":message,"date":date,"time":time})
                EmptyObject["rm"] = "留言成功,感謝您"
                return (EmptyObject);
            else:
                EmptyObject["rm"] = "您留言的次數過多或在短時間內多次留言,請聯絡系統管理員或稍後再進行留言"
        else:
            db.COURSE_MESSAGE.insert_one({"CourseId":CourseId,"Etag":etag,"Message":message,"date":date,"time":time})
        return EmptyObject;
#更新課程留言
def updateCourseMessage(CourseId,etag,message):
        updateMessage = db.COURSE_MESSAGE.update_one({"CourseId":CourseId,"Etag":etag},{"Message":message});
        return ({"message":dumps(updateMessage)})
