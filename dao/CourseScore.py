#課程評分
from bson.json_util import dumps
from mongoengine.connection import connect
from pymongo.mongo_client import MongoClient
from rest_framework.response import Response
from rest_framework.views import APIView
from _functools import reduce


connect(host='m7y3.ddns.net', port=27017)
uri = "mongodb://m7y3CalendarAdmin:nightgossoon90161@m7y3.ddns.net:27017/course_db" 
client = MongoClient(uri)
db = client.course_db
EmptyObject = {"rc":"0000","rm":"您已為此課程評分過,感謝您"};
NoScore = {"rc":"0005","rm":"NoData"}
#更新課程評分
def updateCourseScore(CourseId,etag,score):
    db.COURSE_SCORE.update_one({"CourseId":CourseId,"Etag":etag},{"$set":{"Score":score}})
    return EmptyObject;
#新增課程評分
def insertCourseScore(CourseId,Score,ip,etag):
        courseScore = db.COURSE_SCORE.find({"Etag":etag,"CourseId":CourseId})
        print(courseScore.count())
        if(courseScore.count()<=50):
            db.COURSE_SCORE.insert_one({"CourseId":CourseId,"Score":Score,"IP":ip,"Etag":etag})
            EmptyObject['rm'] = "評分成功,感謝您";
        return EmptyObject;
def queryUserScore(CourseId,etag):
    score = db.COURSE_SCORE.find({"CourseId":CourseId,"Etag":etag});
    return score;
#查詢課程評總分
def queryScore(courseId):  
#         score = db.COURSE_SCORE.find({"CourseId":courseId})
#         db.COURSE_SCORE.reduce1()
#         db.COURSE_SCORE.finalize1()
       
        finalize = """
                     function(prev) {
                            prev.averageScore = prev.sumforaverageaverageScore / prev.countforaverageaverageScore;
                            delete prev.sumforaverageaverageScore;
                            delete prev.countforaverageaverageScore;
                            }
                    """
        reduce =   """
                    function(obj, prev) {
                            prev.sumforaverageaverageScore += obj.Score;
                            prev.countforaverageaverageScore++;
                                }
                   """
        key = {"CourseId": True};
        cond = {"CourseId": courseId}
        initial = {"sumforaverageaverageScore": 0,"countforaverageaverageScore": 0}
        score = db.COURSE_SCORE.group(key,cond,initial,reduce,finalize)
        print(type(score))
        print(score)
#         if ("courseId" in request.data):
#             courseId = request.data["courseId"];
        if score is None or len(score)<= 0:
            print("score is None")
            return {"score":dumps({"averageScore":0})};
        return {"score":dumps(score[0])};