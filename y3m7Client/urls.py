"""y3m7Client URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout
from backend.views import  index
from oauth2client.contrib.django_util.site import urls as oauth2_urls

urlpatterns = [
               url(r'^admin/', admin.site.urls),
               url(r'^y3m7Calendar/',include('y3m7Calendar.urls',namespace="y3m7Calendar")),
               url(r'^$',include('y3m7Calendar.urls',namespace="plus")),
               url(r'^auth_return/',include('y3m7Calendar.urls',namespace="y3m7Calendar")),
               url(r'^plus/',include('plus.urls',namespace="plus")),
               url(r'^oauth2/', include(oauth2_urls)),
               
               #後台
               url(r'^registration/accounts/login/$',login),
               url(r'^index/$',index),
               url(r'^accounts/logout/$',logout),  # 加入此對應
 ]
