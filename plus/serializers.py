from rest_framework_mongoengine.serializers import DocumentSerializer

from plus.models import Notice


class NoticeSerializer(DocumentSerializer):
    class Meta:
        model = Notice
        fields = '__all__'
    def create(self, validated_data):  
        return Notice.objects.create(**validated_data)  
   
    def update(self, instance, validated_data):  
        instance.NoticeId = validated_data.get('noticeId', instance.NoticeId)  
        instance.save()  
        return instance  
