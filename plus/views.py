import datetime

from bson.json_util import dumps
from django.shortcuts import render
from django.views import generic
from mongoengine.connection import connect
from pymongo.mongo_client import MongoClient
from rest_framework.response import Response
from rest_framework.urls import template_name
from rest_framework.views import APIView

from plus.models import Notice


def index(request):
    return render(request, 'plus/index.html')
def ourwork(request):
    return render(request, 'plus/ourwork.html')
def testimonials(request):
    return render(request, 'plus/testimonials.html')
def projects(request):
    return render(request, 'plus/projects.html')
def contact(request):
    return render(request, 'plus/contact.html')
def indexHead(request):
    return render(request, 'plus/indexHead.html')

connect(host='m7y3.ddns.net', port=27017)
uri = "mongodb://m7y3CalendarAdmin:nightgossoon90161@m7y3.ddns.net:27017/course_db" 
client = MongoClient(uri)
db = client.course_db
SYS_NOTICE = db.SYS_NOTICE;
class SysNotice(APIView):
    def get(self,request):
        self.getRemoteIp(request);
        return Response({"notices":self.get_queryset()});
    def get_queryset(self):
        notices = db.SYS_NOTICE.find();
        return dumps(notices,ensure_ascii=False);
    def getRemoteIp(self,request):
        remoteDateTime = datetime.datetime.now().strftime("%Y%m%d %H%M%S").split(" ");
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        date = remoteDateTime[0];
        time = remoteDateTime[1];
        access = None;
        ip = None;
        if x_forwarded_for:
            ip = str(x_forwarded_for.split(',')[-1].strip())
            access = {"page":"index","forward":"FORWARDED_FOR","ip":ip,
                      "date":date,"time":time};
        elif request.META.get('HTTP_X_REAL_IP'):
            ip = str(str(request.META.get('HTTP_X_REAL_IP')))
            access = {"page":"index","forward":"REAL_IP","ip":ip,
                      "date":date,"time":time};
        else:
            ip = str(request.META.get('REMOTE_ADDR'))
            access = {"page":"index","forward":"REMOTE_ADDR","ip":ip,
                      "date":date,"time":time};
        db.ACCESS_LOG.insert_one(access);
        return ip;
class ChangeData(APIView):
    def get(self,request):
        test = self.get_queryset();
        print(test)
        return Response({"changeDatas":test});
    def get_queryset(self):
        changeDatas = db.CHANGE_DATA_LOG.find();
        return dumps(changeDatas,ensure_ascii=False);