'''
Created on 2016年10月15日

@author: equal
'''
import os

from django.conf.urls import url
from django.http import request

from plus import views


urlpatterns = [
                url(r'^$', views.index,name="index"),
#                 url(r'^indexHead.html',views.indexHead.as_view(),name="indexHead"),
                url(r'^index.html',views.index,name="index"),
                url(r'^ourwork.html',views.ourwork,name="ourwork"),
                url(r'^testimonials.html',views.testimonials,name="testimonials"),
                url(r'^projects.html',views.projects,name="projects"),
                url(r'^contact.html',views.contact,name="contact"),
                url(r'^api/queryNotice',views.SysNotice.as_view(),name="queryNotice"),
                url(r'^api/queryChangeData',views.ChangeData.as_view(),name="queryChangeData")
#                 url(r'^oauth2callback', views.auth_return()),
#                 url(r'^accounts/login/$', 'django.contrib.auth.views.login',
#                         {'template_name': 'plus/login.html'}),
#                 url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
#                  {'document_root': os.path.join(os.path.dirname(__file__), 'static')}),


]        