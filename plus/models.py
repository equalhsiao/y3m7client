import mongoengine


class Notice(mongoengine.document.Document):
    noticeId = mongoengine.StringField(max_length=100);
    date = mongoengine.StringField(max_length=100)
    subject = mongoengine.StringField(max_length=100)
    content = mongoengine.StringField(max_length=1000)
    meta = {'collection' : 'notice'}